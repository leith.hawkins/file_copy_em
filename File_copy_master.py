import arcpy
import os
import datetime

logfile = "C:\\Users\\hawkinle\\OneDrive - DPIE\\Emergency Management\\File_Copy\\file_copy.txt"
now = datetime.datetime.now()
date = now.strftime("%d/%m/%y")
time = now.strftime("%H:%M")


print "Start"
f = open(logfile,"w")
f.write( date + "(" + time +")" + "- Process commenced" + "\n")
f.close()


arcpy.env.workspace ="C:\\Users\\hawkinle\\OneDrive - DPIE\\Emergency Management\\File_Copy\\Copied_Data.gdb"
arcpy.env.overwriteOutput = True

FGDB = "C:\\Users\\hawkinle\\OneDrive - DPIE\\Emergency Management\\File_Copy\\Copied_Data.gdb"
HoldingFD = "Holdings_data"
Connector = "\\"

#SDE Databases 
RestrictedDB = "P:\\Resources\\Database_connections\\1_STANDARD_CONNECTIONS\\GIS101Delivery_Restricted.sde"
Lands_Geographics = "P:\\Resources\\Database_connections\\1_STANDARD_CONNECTIONS\\GIS101_Lands_Geographics.sde"
Delivery = "P:\\Resources\\Database_connections\\1_STANDARD_CONNECTIONS\\GIS101Delivery.sde"



Holdings_data = "GIS101DELIVERY_RESTRICTED.DBO.BOUND_ADMIN_HOLDINGS"
Property_Ownership ="GIS101DELIVERY_RESTRICTED.DBO.LAND_OWN_PROPERTY"
LGA_Boundary = "GIS101_Lands_Geographics.dbo.localgovernmentarea"
LLS_Boundary = "GIS101Delivery.DBO.BOUND_ADMIN_Local_Land_Services_Districts"
State_Forest = "GIS101_Lands_Geographics.dbo.stateforest"
NPWS = "GIS101_Lands_Geographics.dbo.npwsreserve"
Roads = "GIS101_Lands_Geographics.dbo.roadnameextent"
Placepoint = "GIS101_Lands_Geographics.dbo.placepoint"
#Holdings to FC 

f = open(logfile,"a")
f.write( date + "(" + time +")" + "- Holdings Process commenced" + "\n")
f.close()
inFeatures = RestrictedDB + Connector + Holdings_data
outLocation = FGDB + Connector + HoldingFD
OutFeatureClass = "Holdings_NSW"
arcpy.FeatureClassToFeatureClass_conversion(inFeatures,outLocation,OutFeatureClass)
f = open(logfile,"a")
f.write( date + "(" + time +")" + "- Holdings Process finished" + "\n")
f.close()

#Lotownership to FC

f = open(logfile,"a")
inFeatures = RestrictedDB + Connector + Property_Ownership
OutFeatureClass="Lot_Ownership"
f.write( date + "(" + time +")" + "- Landownership Process commenced" + "\n")
f.close()
arcpy.FeatureClassToFeatureClass_conversion(inFeatures,outLocation,OutFeatureClass)
f = open(logfile,"a")
f.write( date + "(" + time +")" + "- Landownership Process finished" + "\n")
f.close()
#LGA Region

f = open(logfile,"a")
inFeatures = RestrictedDB + Connector +LGA_Boundary
OutFeatureClass = "LGA_Statewide"
f.write( date + "(" + time +")" + "- LGA Process Commenced" + "\n")
f.close()
arcpy.FeatureClassToFeatureClass_conversion(inFeatures,outLocation,OutFeatureClass)
f = open(logfile,"a")
f.write( date + "(" + time +")" + "- LGA Process Commenced" + "\n")
f.close()

#LLS Boundaries
f = open(logfile,"a")
inFeatures = RestrictedDB + Connector + LLS_Boundary
OutFeatureClass = "LLS_Boundary_Statewide"
f.write( date + "(" + time +")" + "- LLS Process Commenced" + "\n")
f.close()
arcpy.FeatureClassToFeatureClass_conversion(inFeatures,outLocation,OutFeatureClass)
f = open(logfile,"a")
f.write( date + "(" + time +")" + "- LLS Process Commenced" + "\n")
f.close()

#State Forests
f = open(logfile,"a")
inFeatures = RestrictedDB + Connector + stateforest
OutFeatureClass = "State Forests_Statewide"
f.write( date + "(" + time +")" + "- LLS Process Commenced" + "\n")
f.close()
arcpy.FeatureClassToFeatureClass_conversion(inFeatures,outLocation,OutFeatureClass)
f = open(logfile,"a")
f.write( date + "(" + time +")" + "- LLS Process Commenced" + "\n")
f.close()

#National Parks
f = open(logfile,"a")
inFeatures = RestrictedDB + Connector + NPWS
OutFeatureClass = "NPWS_Statewide"
f.write( date + "(" + time +")" + "- LLS Process Commenced" + "\n")
f.close()
arcpy.FeatureClassToFeatureClass_conversion(inFeatures,outLocation,OutFeatureClass)
f = open(logfile,"a")
f.write( date + "(" + time +")" + "- LLS Process Commenced" + "\n")
f.close()

#Roads
f = open(logfile,"a")
inFeatures = RestrictedDB + Connector + Roads
OutFeatureClass = "Roads"
f.write( date + "(" + time +")" + "- LLS Process Commenced" + "\n")
f.close()
arcpy.FeatureClassToFeatureClass_conversion(inFeatures,outLocation,OutFeatureClass)
f = open(logfile,"a")
f.write( date + "(" + time +")" + "- LLS Process Commenced" + "\n")
f.close()

#placepoint
f = open(logfile,"a")
inFeatures = RestrictedDB + Connector + Placepoint
OutFeatureClass = "Placepoint"
f.write( date + "(" + time +")" + "- LLS Process Commenced" + "\n")
f.close()
arcpy.FeatureClassToFeatureClass_conversion(inFeatures,outLocation,OutFeatureClass)
f = open(logfile,"a")
f.write( date + "(" + time +")" + "- LLS Process Commenced" + "\n")
f.close()